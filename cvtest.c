#include "types.h"
#include "user.h"

typedef struct {
    volatile char value;
    mutex_t mutex;
    cond_var_t cv_odd;
    cond_var_t cv_even;
} sync_t;

void write_val_was(int n)
{
    char buf[15];
    int len = strlen("Value was ");
    memmove(buf, "Value was ", len);
    buf[len] = n + 0x30;
    buf[len + 1] = ':';
    buf[len + 2] = ' ';
    buf[len + 3] = 0;
    write(1, buf, strlen(buf));
}

void put_one(sync_t *sync)
{
    mutex_lock(&sync->mutex);
    while (sync->value % 2 != 0) {
        write_val_was(sync->value);
        write(1, "Put even sleeping\n", strlen("Put even sleeping\n"));
        cv_wait(&sync->cv_even, &sync->mutex);
        write(1, "Put even rechecking the value\n", strlen("Put even rechecking the value\n"));
    }
    write(1, "Put even awake\n", strlen("Put even awake\n"));
    sync->value++;
    mutex_unlock(&sync->mutex);
    cv_bcast(&sync->cv_odd);
}

void put_one_odd(sync_t *sync)
{
    mutex_lock(&sync->mutex);
    while (sync->value % 2 == 0) {
        write_val_was(sync->value);
        write(1, "Put odd sleeping\n", strlen("Put odd sleeping\n"));
        cv_wait(&sync->cv_odd, &sync->mutex);
        write(1, "Put odd rechecking the value\n", strlen("Put odd rechecking the value\n"));
    }
    write(1, "Put odd awake\n", strlen("Put odd awake\n"));
    sync->value++;
    mutex_unlock(&sync->mutex);
    cv_bcast(&sync->cv_even);
}

int main(void)
{
    int i, num_threads = 3;

    sync_t *sync = (sync_t *)shmbrk(sizeof(sync_t));
    mutex_init(&sync->mutex);
    cv_init(&sync->cv_even);
    cv_init(&sync->cv_odd);
    sync->value = 0;
    for (i = 0; i < num_threads; i++) {
        if (fork() == 0) {
            put_one(sync);
            exit();
        }
        if (!fork()) {
            put_one_odd(sync);
            exit();
        }
    }
    // clang-format off
    while (wait() > 0); // boo fucking hoo divjot the semi can go on the next line
    // clang-format on
    printf(1, "Value: %d\n", sync->value);
    exit();
}
