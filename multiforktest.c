#include "types.h"
#include "user.h"

// simple test that our initial syscall is correct
void test1()
{
    int ref_count = refcount(test1);
    assert_eq(ref_count, 1, "ref count not one!");
}

// check that the reference count is two after a single fork
void test2()
{
    int pid = fork();
    int ref_count;

    if (pid) {
        ref_count = refcount(test2);
        assert_eq(ref_count, 2, "ref count not two!");
    } else
        exit();
    wait();
}
char y;

// assert that ref count properly increments on forks
void test3()
{
    if (fork() == 0) {
        if (fork() == 0) {
            assert_eq(refcount(&y), 3, "Refcount of y is not 3");
            exit();
        }
        wait();
        exit();
    }
    wait();
}

char z;

// assert that ref count properly decrements on child death
void test4()
{
    if (fork() == 0) {
        assert_eq(refcount(&z), 2, "ref count of z is not 2");
        exit();
    }
    wait();
    assert_eq(refcount(&z), 1, "ref count of z is not 1");
}

char a;

// extensive test of decrementing
void test5()
{
    if (fork() == 0) {
        if (fork() == 0) {
            assert_eq(refcount(&a), 3, "ref count of a is not 3");
            exit();
        }
        wait();
        assert_eq(refcount(&a), 2, "ref count of a is not 2");
        exit();
    }
    wait();
    assert_eq(refcount(&a), 1, "ref count of a is not 1");
}

char b;

// test that page fault handler is correct
void test6()
{
    if (fork() == 0) {
        b = 1;
        assert_eq(refcount(&b), 1, "ref count of b is not 1 after writing to it");
        exit();
    }
    wait();
    assert_eq(refcount(&b), 1, "ref count of b is not 1 after the child wrote to it");
}

typedef void (*test_func)();

#define TEST_COUNT 6

static const test_func tests[] = { test1, test2, test3, test4, test5, test6 };

int main(void)
{
    int i;
    for (i = 0; i < TEST_COUNT; i++) {
        printf(1, "--- RUNNING TEST %d ---\n", i + 1);
        tests[i]();
        printf(1, "--- TEST %d PASSED  ---\n\n", i + 1);
    }
    exit();
}
