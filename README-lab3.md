# Lab 3 README: Brian Gavin & Divjot Arora

## Changes by File

### trap.c

Old functionality of `page_fault_handler` moved into a new function named 
`uncow_page` which is defined in vm.c. `page_fault_handler` now functions to 
either uncow a page or to extend the stack in valid cases. On timer inturrupts, 
the `inc_vdso_ticks` function is called. 

### proc.h

`struct proc` modified to now hold stack VMA fields `stack_start` `stack_bound` 
and `stack_end`. `stack_start` marks the address of the first page of the stack.
`stack_end` marks the address of the final guard page of the stack. `stack_bound` 
marks the address of the current guard page of the stack. If a stack access is 
within the bound `stack_bound` to `stack_bound + PGSIZE` it is a stackoverflow.

### proc.c

`invalid_stack` function is used to validate that a given user virtual address is
in a present page in the stack VMA of the user program. If the address is not 
on the stack, return 0, if the address is within the bounds of the stack and 
valid, return 0 and if the address is within the bounds of the stack and 
invalid, return 1.

`stack_overfow` function is used to check whether a stack overflow has occured
from the given address in the current process context. This checks the condition
outlined in the `proc.h` section.

### vm.c

`cowuvm` updated to "skip" the unused parts of the address space inside of the 
stack VMA. (The pages between `end` and `bound + PGSIZE`)

`iscow` function used to check whether a page for a given user VA is COW. This
is used by syscalls that write to a user page to check whether they must first
uncow the page before writing.

`invalidate_first_upage` function used to make the first page in the page table
not present. This gives user programs a null pointer functionality, as 
dereferencing a pointer with value 0 will cause a page fault and kill the 
program.

`extend_stack` function will extend the stack for a given process by one page,
and returns 0 or 1 depending on if the stack was extended. If the process is 
attempting to extend into the final guard page (address `stack_end`) it will 
return 1. This will result in the process being killed.

`extend_stack` will allocate the new stack page only if the guard page it is
consuming has not already been allocated. The first guard page is allocated
by `exec`. It will not allocate any subsequent guard pages, only change the 
`stack_bound` variable.

Stack extension is only done when the process attempts to grow into the guard
page, but not past it. For example, an allocation of 0x2000 bytes on the stack
will result in the process being killed. The stack will not iteratively grow 
until the size request is met.

`alloc_vdso` completed.

### exec.c

Stack repositioning implemented here, and invalidating the first page of the new 
process. 

### memlayout.h

Macro `FIRST_UVA` defined as `0x1000`

### syscall.c

`fetchint`, `fetchstr` and `argptr` updated to assert that the given address
is valid for a user program (after `init`) by checking that it is above 
`FIRST_UVA`.

`fetchstr` and `argptr` updated to assert that the given address is a valid
stack address by calling `invalid_stack`.

## New Syscalls

`stack_start` `stack_bound` and `stack_end` added to return that value for the
calling process' context. Used for testing.

## Tests

Test files `stackoverflow.c` and `vdsotests.c` used to test stack extention and
the new VDSO syscalls.

`np_read.c` and `np_write.c` used to test null pointer operations.
 
