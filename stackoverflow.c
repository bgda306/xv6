#include "types.h"
#include "user.h"
#include "debug.h"

void test_bigger_stack_frame(void);
int bigger_stack_frame(int);
void test_fork(void);
void syscall_test(void);
void large_single_allocation(void);

int main(void)
{
    printf(1, "Only one process should die...");
    if (!fork()) {
        large_single_allocation();
        printf(1, "Large single allocation test failed!\n");
        exit();
    }
    wait();
    if (!fork()) {
        test_bigger_stack_frame();
        exit();
    }
    wait();
    if (!fork()) {
        test_fork();
        exit();
    }
    wait();
    if (!fork()) {
        syscall_test();
        exit();
    }
    wait();
    printf(1, "Stack extension tests passing.\n");
    exit();
}

void test_fork(void)
{
    bigger_stack_frame(1);
    int pstart, pbound, pend;

    stack_start((char *)&pstart, 4);
    stack_bound((char *)&pbound, 4);
    stack_end((char *)&pend, 4);
    if (!fork()) {
        int bound, ref_count;

        stack_bound((char *)&bound, 4);
        assert_eq(bound, pbound, "Bound changed after fork");

        ref_count = refcount((void *)(bound + 0x1000));
        assert_eq(ref_count, 2, "Ref count not 2");
        assert_eq(refcount((void *)(bound + 0x2000)), 1, "Ref count of first page not 1");
        exit();
    }
    wait();
}

void large_single_allocation(void)
{
    volatile char a[0x2000];
    a[0] = 1;
    (void)a;
}

void syscall_test(void)
{
    if (!fork()) {
        // read into garbage address in stack VMA
        int rv = read(1, (void *)0xffff, 2);
        assert_eq(rv, -1, "Read should have gracefully failed");
        exit();
    }
    wait();
    if (!fork()) {
        int rv = open((char *)0xffff, 0);
        assert_eq(rv, -1, "Open should have gracefully failed");
        exit();
    }
    wait();
}

void test_bigger_stack_frame(void)
{
    int ostart, obound, nstart, nbound;

    stack_start((char *)&ostart, 4);
    stack_bound((char *)&obound, 4);
    bigger_stack_frame(1);
    stack_start((char *)&nstart, 4);
    stack_bound((char *)&nbound, 4);
    assert_eq(ostart, nstart, "Stack start should not have changed");
    assert_eq(obound - 0x1000, nbound, "Bound not decremented by one page size");
}

int bigger_stack_frame(int call_num)
{
    if (call_num == 0)
        return 0;
    const int size = 4097;
    volatile char a[size];
    a[0] = 1;
    (void)a;
    return bigger_stack_frame(call_num - 1);
}
