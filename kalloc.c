// Physical memory allocator, intended to allocate
// memory for user processes, kernel stacks, page table pages,
// and pipe buffers. Allocates 4096-byte pages.

#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "spinlock.h"

#define MAXPAGES (PHYSTOP / PGSIZE)

void freerange(void *vstart, void *vend);

extern char end[]; // first address after kernel loaded from ELF file
// defined by the kernel linker script in kernel.ld

struct run {
    struct run *next;
    uint ref_count;
};

struct {
    struct spinlock lock;
    int use_lock;
    struct run *freelist;
    // Lab 2: For COW fork, we can't store the run in the
    // physical page, because we need space for the ref
    // count.  Moving the data to the kmem struct.
    struct run runs[MAXPAGES];
} kmem;

// translate a run pointer to a virtual address.
// if null it will return null
static char *run_to_virt(struct run *r)
{
    char *rv;
    rv = r ? P2V((r - kmem.runs) * PGSIZE) : r;
    return rv;
}

// translate virtual address to run pointer
static struct run *virt_to_run(void *v)
{
    return &kmem.runs[(V2P(v) / PGSIZE)];
}

// increment atomic instruction wrapper for small optimizations
static inline void increment_ref_count_run(struct run *page)
{
    __atomic_fetch_add(&page->ref_count, 1, __ATOMIC_SEQ_CST);
}

// increment ref count for a given page at va
inline void increment_ref_count(void *va)
{
    struct run *page;
    page = virt_to_run(va);
    increment_ref_count_run(page);
}

// decrement atomic instruction wrapper for small optimazations
static inline void decrement_ref_count_run(struct run *page)
{
    __atomic_fetch_sub(&page->ref_count, 1, __ATOMIC_SEQ_CST);
}

// decrement ref count for the page at va
inline void decrement_ref_count(void *va)
{
    struct run *page;
    page = virt_to_run(va);
    decrement_ref_count_run(page);
}

uint get_ref_count_va(void *va)
{
    struct run *r;
    uint pc;
    r = virt_to_run(va);
    pc = __sync_add_and_fetch(&r->ref_count, 0);
    return pc;
}

// Initialization happens in two phases.
// 1. main() calls kinit1() while still using entrypgdir to place just
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.

void kinit1(void *vstart, void *vend)
{
    initlock(&kmem.lock, "kmem");
    kmem.use_lock = 0;
    freerange(vstart, vend);
}

void kinit2(void *vstart, void *vend)
{
    freerange(vstart, vend);
    kmem.use_lock = 1;
}

void freerange(void *vstart, void *vend)
{
    char *p;
    p = (char *)PGROUNDUP((uint)vstart);
    for (; p + PGSIZE <= (char *)vend; p += PGSIZE)
        kfree(p);
}

// Free the page of physical memory pointed at by v,
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void kfree(char *v)
{
    struct run *r;

    if ((uint)v % PGSIZE)
        panic("kfree: v % PGSIZE");
    if (v < end) {
        panic("kfree: v < end");
    }
    if (V2P(v) >= PHYSTOP)
        panic("kfree: v >= PHYSTP");

    // no panic
    if (kmem.use_lock)
        acquire(&kmem.lock);

    r = virt_to_run(v);
    // Fill with junk to catch dangling refs.
    memset(v, 1, PGSIZE);
    // Lab2: because we moved 'runs' to kmem
    // r = (struct run*)v;
    r->next = kmem.freelist;
    r->ref_count = 0;
    kmem.freelist = r;
    if (kmem.use_lock)
        release(&kmem.lock);
}

// kfree with resepct to the reference count
void kfree_ref_count(char *v)
{
    struct run *r;

    // panic conditions
    if ((uint)v % PGSIZE)
        panic("kfree_ref_count: v % PGSIZE");
    if (v < end) {
        panic("kfree_ref_count: v < end");
    }
    if (V2P(v) >= PHYSTOP)
        panic("kfree_ref_count: v >= PHYSTP");

    if (kmem.use_lock)
        acquire(&kmem.lock);

    // check reference count of page
    r = virt_to_run(v);
    if (r->ref_count == 0) {
        panic("kfree_ref_count: ref count zero");
    }

    // decrement
    if (r->ref_count > 1) {
        decrement_ref_count_run(r);
        if (kmem.use_lock)
            release(&kmem.lock);
    }

    // free
    else {
        if (kmem.use_lock)
            release(&kmem.lock);
        kfree(v);
    }
}

// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char *kalloc(void)
{
    struct run *r;
    char *rv;

    if (kmem.use_lock)
        acquire(&kmem.lock);

    r = kmem.freelist;
    rv = run_to_virt(r);
    if (r) {
        kmem.freelist = r->next;
        increment_ref_count(rv);
    }

    if (kmem.use_lock)
        release(&kmem.lock);

    // Lab2: because we moved 'runs' to kmem
    // return (char*)r;
    return rv;
}
