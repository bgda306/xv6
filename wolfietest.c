#include "types.h"
#include "user.h"
#include "debug.h"

int main(int argc, char **argv)
{
    printf(1, "%x\n", *(int *)0x103000);
    size_t size = 1000;
    // char c[size];
    char *c = 0;
    int rv;
    rv = wolfie(c, size);
    if (rv < 0)
        printf(1, "wolfie returned %d\n", rv);
    else
        printf(1, "%s\n", c);
    exit();
}
