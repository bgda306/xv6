#include "types.h"
#include "user.h"

#define TRY 0
#define LOCK 1
#define NUM_PROC 20

// mutex test
typedef struct {
    int val;
    mutex_t mutex;
} sync_val_t;

void _mutex_test(int mode)
{
    int i;
    sync_val_t *val;

    if (mode != TRY && mode != LOCK)
        return;

    val = (sync_val_t *)shmbrk(sizeof(sync_val_t));
    val->val = 0;
    mutex_init(&val->mutex);
    for (i = 0; i < NUM_PROC; ++i) {
        if (!fork()) {
            if (mode == LOCK)
                mutex_lock(&val->mutex);
            else
                while (mutex_trylock(&val->mutex))
                    ;
            val->val++;
            mutex_unlock(&val->mutex);
            exit();
        }
    }
    for (i = 0; i < NUM_PROC; ++i)
        wait();
    printf(1, "val: %d\n", val->val);
    assert_eq(val->val, NUM_PROC, "Races between the processes!");
}

void mutex_lock_test()
{
    _mutex_test(LOCK);
}

void mutex_try_test()
{
    _mutex_test(TRY);
}

int main(void)
{
    mutex_lock_test();
    mutex_try_test();
    exit();
}
