#include "types.h"
#include "user.h"
#include "debug.h"

#define NUM_ITERATIONS 10

void pidtest(void);
void tickstest(void);

int main(int argc, char **argv)
{
    pidtest();
    tickstest();
    printf(1, "Pid and ticks tests passed.\n");
    exit();
}

void pidtest(void)
{
    int syspid = getpid();
    int vdsopid = vdso_getpid();

    assert_eq(syspid, vdsopid, "Pid not the same in VDSO version of syscall.");
}

void tickstest(void)
{
    int oldticks = vdso_getticks();
    sleep(1);
    int currticks = vdso_getticks();
    assert(currticks > oldticks, "Ticks not increasing");
}
