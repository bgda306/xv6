# Lab 4 READMEME: Brian Gavin and Divjot Arora

## Changes by File

## proc.h

Add shmarea (pronounced shmary-uh) fields to process struct: start and brk.

## exec.c

Add shminitialization to shmarea values to be pages right after the stack with shminitial size of 0.
Set the size of the process to include the max size of the shmarea, defined by MAX_SHM.

## mmu.h

New PTE flag `PTE_SHM` defined as `0x400`.

## vm.c

New function `allocuvm2` has the same functionality as `allocuvm` but the PTE flags of the new pages can be specified with the argument `flags`.

`grow_shmarea` implements the functionality of `shmbrk`.

It will grow the shmarea by one page rounded up from the input argument if the argument is positive and the input value will not cause an overflow.
The pages are allocated with `allocuvm2`, and use the new PTE flag `PTE_SHM` to specify they are SHM pages.

The entire shmarea is decallocated using `deallocuvm` if the input argument is negative.

If the input argument is 0, the process' current shmbrk is returned.

`cowuvm` is updated to support the shmarea by skipping over the non-present shmarea pages.
For a present shmarea page (has flag `PTE_SHM`) it skips over the cow-ing of the page and simply increments the reference count.

## proc.c

`invalid_shmarea` will check if a given user virtual address is a valud shmarea address. This is called in the syscall argument checking functions.

`futex_wait` implements futex functionality.

## user.h

We add the fields for the mutex and the condition variable, a single volatile integer that is modified via the futex syscalls.

The double underscore denotes the 'private' nature of this struct member.
Also because using double underscores makes me feel like a cool library author.

## ulib.c

Here our mutex and condition variable are implemented.
The init functions for these simply set the value of the futex to zero.

### Mutex Implementation

`mutex_lock` Uses an atomic exchange to set the value of the futex to one.
While it was already one, wait on the futex for it to change to zero.

`mutex_trylock` This function is one line and works.

`mutex_unlock` Set the futex to the unlocked state (zero) and wake the threads sleeping on this futex.

### CV Implementation

The futex for the CV works sort of as a one-way ticket lock.
`cv_wait` atomically loads the value of the futex at that moment, unlocks the mutex and waits the futex on that value.
`cv_bcast` will increment the futex value and wake all threads sleeping on the value.

This ensures that `wait` is atomic with respect to `bcast` because a `bcast` call interleaved with a wait will cause the `futex_wait` to instantly return.
Of course, good programming practice will not cause this sitation to happen.

## New Syscalls

`sys_shmbrk` will call `grow_shmarea` with the argument to the syscall.

`sys_futex_wait` will call `futex_wait` with the two arguments to the syscall.

`sys_futex_wait` will call `wakeup` with the argument to the syscall.

## Tests

### `shmarea_test`
Tests that `shmbrk` will grow the shmarea, will free the shmarea, that the shmarea actually shares memory between parent and child processes, and the robustness of the `shmbrk` syscall.

### `mt` (Mutex Test)

Simple test to test mutual exclusion of a single variable.

### `cvtest` (Condition Variable Test)

Tests proper ordering, and more throurough mutual exclusion, of the condition variables.
The test uses `write` for all output, which is not buffered.
This invariant helped us to find a bug in our mutex since in one ordering of the output, the writes were interleaved.
We discovered that our mutex was never actually locking, as `mutex_lock` was writing 0 to the futex value.

### `checksum`

Implement a simple circular bounded buffer with a single mutex and two condition variables for consuming and producing.
