#ifndef DEBUG_H
#define DEBUG_H

#define CBLU "\e[34m"
#define CEND "\e[0m"

#ifdef DEBUG
#define debug(fmt, ...)                                                        \
    ({                                                                         \
        printf(2, CBLU "DEBUG: %s::%s::%d " CEND fmt "\n", __FILE__,	       \
            __extension__ __FUNCTION__, __LINE__, ##__VA_ARGS__);              \
    })
#else
#define debug(fml, ...)
#endif // DEBUG

#endif // DEBUG_H
