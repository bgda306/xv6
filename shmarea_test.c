#include "types.h"
#include "user.h"
#include "debug.h"

// test that shm_brk does properly grows shmarea and is writable
void test_grows(void)
{
    char *brk = shmbrk(20);
    *brk = 'a';
    debug("brk: %p", brk);
    debug("*brk: %x", *brk);
    assert_eq(*brk, 'a', "Shmarea not written to");
    printf(1, "test_grows passed\n");
    exit();
}

void test_frees(void)
{
    char *old_brk, *new_brk;
    old_brk = shmbrk(20);
    *old_brk = 'a';
    new_brk = shmbrk(-1);
    *new_brk = 'a'; // this should kill the proc
    assert(0, "Shmarea test failed, program did not crash");
    exit();
}

void test_shared(void)
{

    char *brk = shmbrk(20);
    if (!fork()) {
        *brk = 'a';
        exit();
    }
    wait();
    assert_eq(*brk, 'a', "Shmarea not written to");
    printf(1, "test_shared passed\n");
    exit();
}

void test_shared2(void)
{
    char *brk = shmbrk(20);
    if (!fork()) {
        *brk = 'a';
        if (!fork()) {
            assert_eq(brk[0], 'a', "Write in child not seen in grandchild");
            brk[1] = 'a';
            exit();
        }
        wait();
        assert_eq(brk[1], 'a', "Write in grandchild not seen in child");
        exit();
    }
    wait();
    assert_eq(brk[0], 'a', "Write in child not seen in parent");
    assert_eq(brk[1], 'a', "Write in grandchild not seen in parent");
    exit();
}

void test_syscalls(void)
{
    int rv, fds[2], i, buf_size = 100;
    char buf[buf_size];
    char *brk = shmbrk(20);

    for (i = 0; i < buf_size; ++i)
        buf[i] = 'a';

    rv = pipe(fds);
    if (rv < 0) {
        printf(1, "pipe failed\n");
        exit();
    }
    write(fds[1], buf, buf_size);
    rv = read(fds[0], brk, buf_size);
    assert(rv > 0, "Read failed on valid shmarea");
    for (i = 0; i < buf_size; ++i) {
        assert_eq(buf[i], brk[i], "Buf not copied into shmarea");
    }
    brk = shmbrk(-1);
    write(fds[1], buf, buf_size);
    rv = read(fds[0], brk, buf_size);
    assert_eq(rv, -1, "Read did not fail when shmarea invalid");
    exit();
}

int main(void)
{
    printf(1, "SHMAREA TESTS\n");
    printf(1, "Test that shmarea will grow\n");
    if (!fork()) {
        test_grows();
    }
    wait();

    printf(1, "Test that shmarea shrinks, process should crash...");
    if (!fork()) {
        test_frees();
    }
    printf(1, "\n");
    wait();

    printf(1, "Test that shamrea is shared between parent and child\n");
    if (!fork()) {
        test_shared();
    }
    wait();

    if (!fork()) {
        test_shared2();
    }
    wait();
    printf(1, "Shmarea sharing tests passed\n");

    printf(1, "Testing robustness of syscalls.\n");
    if (!fork()) {
        test_syscalls();
    }
    wait();
    printf(1, "Syscall tests passed\n");
    printf(1, "ALL TESTS PASSED\n");
    exit();
}
