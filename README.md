# Lab2 README: Brian Gavin & Divjot Arora

## Additions

### Makefile additions

New targets `debug`, `nocow` and `nocow-debug` have been added. 

### New Files

We added new files: `kdebug.h`, `debug.h`, `pagefault_test.c`, `multiforktest.c`

The first two files add macros that are conditionally defined as wrappers for
`cprintf` and `printf`. The condition is the definition of the macro `KDEBUG` and 
`DEBUG`, for which we added a new target to the Makefile, `debug` that will 
add `-DKDEBUG -DDEBUG` to the `C_FLAGS` variable before the `xv6.img` target is
executed. The logging macros are very useful for debugging; our main debugging
strategy is based upon print statements, and we have both benefitted from this
strategy in the past.

`pagefault_test.c` is a user program that accesses an invalid page to force a page 
fault.

`multiforktest.c` contains our tests for the new fork implementation. It makes use of
the assert library, and runs multiple tests. The test conditions are documented
in the source file.

### `kalloc.c` additions

We added a reference count variable to `struct run` and static helper functions for
incrementing and decrementing this variable, given a `struct run*`. The functions
use atomic instructions via GCC's `__sync_op_and_fetch()` wrappers.

To avoid having to give global visibilty to the `struct run` the globally visibly
incrementing and decrementing functions take a virtual address as an argument. We 
added wrappers to translate a virtual address to the respctive physical page it is
associated with.

`kfree_ref_count` is a new function that will only free a page that has a refernce 
count of 1. In the case where the page has multiple references, it will only decrement 
the page count. If `kfree_ref_count` is given a page that has 0 refence counts, 
it will panic

`kfree` operates as normal, freeing the input page.

`kalloc` will increment the page count of the page it returns, which is guaranteed
to be zero, due to `kfree`'s new panic condition.

### `vm.c`

`walkpgdir` was given global scope such that it can be used in the new trap handler
(documented below).

`deallocuvm` now calls `kfree_ref_count` on the pages it deallocs, as the user 
pages it attempts to free are dependant upon the reference count. The other 
`free`-type functions utilize the usual `kfree` as they are freeing either page 
directories or page tables, which are per-process and do not care about reference 
counts.

`cowuvm` is the new function that implements copy-on-write fork. For each page
table entry that it finds in the parent process' page tables, if it is a writeable
page, it will set the entry with the new flag `PTE_COW` to desinate this page as
copy on write and read-only, and map the same physical page that this entry 
references to the child process' page tables. The reference count of the physical
page is incremented.

`cowuvm` will panic if the pte is neither exists nor it is not present.
Additionally, it will shootdown the TLB entries for each PTE.

### `proc.c` additions

`fork` has been updated to conditionally call `copyuvm` or `cowuvm` depending on
the compilation method. The Makefile's `nocow` type targets add `-DNOCOW` to the
`C_FLAGS` before compilation. If `NOCOW` is defined, fork will call `copyuvm`
instead of `cowuvm`.

### `trap.c` additions

`trap` now has a new case for handling a page fault. It will call the new function
`page_fault_handler`. If the function returns nonzero, it will kill the process.

`page_fault_handler` is a static function to handle a page fault. We chose to give
the minimum visibilty for the function. After the faulting address has been
validated, COW functionality is implemented. 

### New system call

A new system call was added to userspace, `int refcount(void*)` which will return the
reference count of the physical page associated with the input user virtual address

### `ulibc.c` additions

For testing, a small assert library was added, `assert(int, const char*)`, 
`assert_eq(int, int, const char*)` and `assert_not(int, const char*)`. These functions
will test the input condition, or if the two values are equal, and a message associated
with the assert. If the assert fails, the process is killed.

