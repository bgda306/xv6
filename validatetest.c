#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

int stdout = 1;

// TODO: make sure this works now that we moved the stack to a different part
// of the address space
void validateint(int *p)
{
    int res;
    asm("mov %%esp, %%ebx\n\t"
        "mov %3, %%esp\n\t"
        "int %2\n\t"
        "mov %%ebx, %%esp"
        : "=a"(res)
        : "a"(SYS_sleep), "n"(T_SYSCALL), "c"(p)
        : "ebx");
}

int main(int argc, char **argv)
{
    int hi, pid;
    uint p;

    printf(stdout, "validate test\n");
    hi = 1100 * 1024;

    for (p = 0; p <= (uint)hi; p += 4096) {
        if ((pid = fork()) == 0) {
            // try to crash the kernel by passing in a badly placed integer
            validateint((int *)p);
            exit();
        }
        sleep(0);
        sleep(0);
        kill(pid);
        wait();
        printf(stdout, "validateints did not crash: %d\n", p / 4096);
        printf(stdout, "giving addr %p to link\n", p);
        // try to crash the kernel by passing in a bad string pointer
        if (link("nosuchfile", (char *)p) != -1) {
            printf(stdout, "link should not succeed\n");
            exit();
        }
    }

    printf(stdout, "validate ok\n");
    exit();
}
