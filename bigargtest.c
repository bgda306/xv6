#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

const int stdout = 1;

void bigargtest(void)
{
    int pid, fd;

    unlink("bigarg-ok");
    pid = fork();
    if (pid == 0) {
        static char *args[MAXARG];
        int i;
        for (i = 0; i < MAXARG - 1; i++)
            args[i] = "bigargs test: failed\n                                  "
                      "                                                        "
                      "                                                        "
                      "                                                     ";
        args[MAXARG - 1] = 0;
        printf(stdout, "bigarg test\n");
        exec("echo", args);
        printf(stdout, "bigarg test ok\n");
        fd = open("bigarg-ok", O_CREATE);
        close(fd);
        exit();
    } else if (pid < 0) {
        printf(stdout, "bigargtest: fork failed\n");
        exit();
    }
    wait();
    fd = open("bigarg-ok", 0);
    if (fd < 0) {
        printf(stdout, "bigarg test failed!\n");
        exit();
    }
    close(fd);
    unlink("bigarg-ok");
}

int main(void)
{
    bigargtest();
    exit();
}
