#include "types.h"
#include "stat.h"
#include "fcntl.h"
#include "user.h"
#include "x86.h"
#include "vdso.h"
#include "debug.h"

char *strcpy(char *s, char *t)
{
    char *os;

    os = s;
    while ((*s++ = *t++) != 0)
        ;
    return os;
}

int strcmp(const char *p, const char *q)
{
    while (*p && *p == *q)
        p++, q++;
    return (uchar)*p - (uchar)*q;
}

uint strlen(char *s)
{
    int n;

    for (n = 0; s[n]; n++)
        ;
    return n;
}

void *memset(void *dst, int c, uint n)
{
    stosb(dst, c, n);
    return dst;
}

char *strchr(const char *s, char c)
{
    for (; *s; s++)
        if (*s == c)
            return (char *)s;
    return 0;
}

char *gets(char *buf, int max)
{
    int i, cc;
    char c;

    for (i = 0; i + 1 < max;) {
        cc = read(0, &c, 1);
        if (cc < 1)
            break;
        buf[i++] = c;
        if (c == '\n' || c == '\r')
            break;
    }
    buf[i] = '\0';
    return buf;
}

int stat(char *n, struct stat *st)
{
    int fd;
    int r;

    fd = open(n, O_RDONLY);
    if (fd < 0)
        return -1;
    r = fstat(fd, st);
    close(fd);
    return r;
}

int atoi(const char *s)
{
    int n;

    n = 0;
    while ('0' <= *s && *s <= '9')
        n = n * 10 + *s++ - '0';
    return n;
}

void *memmove(void *vdst, void *vsrc, int n)
{
    char *dst, *src;

    dst = vdst;
    src = vsrc;
    while (n-- > 0)
        *dst++ = *src++;
    return vdst;
}

#define _assert_failed(s)                                                                                              \
    ({                                                                                                                 \
        printf(2, "ASSERTION FAILED: %s\n", s);                                                                        \
        kill(getpid());                                                                                                \
    })
#define _assert_passed() ({ return; })

void assert(int cond, const char *msg)
{
    cond ? _assert_passed() : _assert_failed(msg);
}

void assert_eq(int i1, int i2, const char *msg)
{
    assert(i1 == i2, msg);
}

void assert_not(int cond, const char *msg)
{
    assert(!cond, msg);
}

uint vdso_getticks()
{
    static vdso_getticks_t _getticks_func = 0;

    // upon the first use, get the entry from the kernel
    if (0 == _getticks_func) {
        _getticks_func = vdso_entry(VDSO_GETTICKS);
    }

    // call the function
    return _getticks_func();
}

uint vdso_getpid()
{
    static vdso_getpid_t _getpid_func = 0;

    // upon the first use, get the entry from the kernel
    if (0 == _getpid_func) {
        _getpid_func = vdso_entry(VDSO_GETPID);
    }

    // call the function
    return _getpid_func();
}

// set mutex lock value to unlocked state: initially unlocked
void mutex_init(mutex_t *l)
{
    l->__lock = 0;
}

// lock the mutex: wait on futex if we did not acquire it
void mutex_lock(mutex_t *l)
{
    while (__atomic_exchange_n(&l->__lock, 1, __ATOMIC_SEQ_CST) == 1)
        futex_wait((void *)&l->__lock, 1);
}

// return 0 on success, return -1 on failure
int mutex_trylock(mutex_t *l)
{
    // think about it...
    return 0 - __atomic_exchange_n(&l->__lock, 1, __ATOMIC_SEQ_CST);
}

// unlock: wake up all sleeping processes
void mutex_unlock(mutex_t *l)
{
    __atomic_store_n(&l->__lock, 0, __ATOMIC_SEQ_CST);
    futex_wake((void *)&l->__lock);
}

void cv_init(cond_var_t *cv)
{
    cv->__futex = 0;
}

void cv_wait(cond_var_t *cv, mutex_t *m)
{
    volatile int val;
    val = __atomic_load_n(&cv->__futex, __ATOMIC_SEQ_CST);
    mutex_unlock(m);
    futex_wait((void *)&cv->__futex, val);
    mutex_lock(m);
}

void cv_bcast(cond_var_t *cv)
{
    __atomic_fetch_add(&cv->__futex, 1, __ATOMIC_SEQ_CST);
    futex_wake((void *)&cv->__futex);
}
