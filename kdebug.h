#ifndef KDEBUG_H
#define KDEBUG_H

#define CMAG "\e[35m"
#define CEND "\e[0m"

#ifdef KDEBUG

// debug print statement
 #define kdebug(fmt, ...)                                                      \
    ({                                                                         \
        cprintf(CMAG "KDEBUG: %s::%s::%d " CEND fmt "\n", __FILE__,            \
            __extension__ __FUNCTION__, __LINE__, ##__VA_ARGS__);              \
    })

// conditionally compiled if condition for debug
 #define kdebug_if(cond) if(cond)

#else
 #define kdebug(fml, ...)
 #define kdebug_if(cond)
#endif // KDEBUG

#endif // KDEBUG_H