;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")n
((c-mode
  (flycheck-gcc-language-standard . "gnu11")
  (flycheck-gcc-include-path . ("."))
  (flycheck-gcc-includes . ("./types.h"))
  (flycheck-gcc-args . ("-fno-pic" "-static" "-fno-builtin" "-fno-strict-aliasing" "-O2" "-MD" "-m32" "-fno-omit-frame-pointer" "-nostdinc"))
))
