#include "types.h"
#include "user.h"
#include "fcntl.h"
#include "debug.h"

#define NUM_CONSUMERS 4
#define NUM_PRODUCERS NUM_CONSUMERS
#define SINGLE_CHECKSUM 196217
#define NUM_INSERTED 12
#define NUM_CONSUMED 8
#define BUFFER_SIZE 128

typedef struct buffer {
    char buf[BUFFER_SIZE];
    int head;
    int tail;
    int checksum;
    int size;
    int num_producers;
    mutex_t mutex;
    cond_var_t full_cv;
    cond_var_t empty_cv;
} buffer_t;

void buffer_init(buffer_t *);
void produce(buffer_t *);
void consume(buffer_t *);

int main()
{
    int i;
    buffer_t *buffer = (buffer_t *)shmbrk(sizeof(buffer_t));
    buffer_init(buffer);

    for (i = 0; i < NUM_CONSUMERS; i++) {
        if (!fork()) {
            consume(buffer);
            exit();
        }
    }
    for (i = 0; i < NUM_PRODUCERS; i++) {
        if (!fork()) {
            produce(buffer);
            exit();
        }
    }
    while (wait() > 0)
        ;

    printf(1, "Checksum: %d\n", buffer->checksum);
    assert_eq(buffer->checksum, SINGLE_CHECKSUM * NUM_PRODUCERS, "Checksum is not correct");
    exit();
}

void buffer_init(buffer_t *buffer)
{
    buffer->head = 0;
    buffer->tail = 0;
    buffer->checksum = 0;
    buffer->size = 0;
    buffer->num_producers = NUM_PRODUCERS;
    mutex_init(&buffer->mutex);
    cv_init(&buffer->empty_cv);
    cv_init(&buffer->full_cv);
}

void produce(buffer_t *buffer)
{
    int fd = open("README", O_RDONLY);

    while (1) {
        // read before locking
        char temp[NUM_INSERTED];
        int bytes_read, i;

        bytes_read = read(fd, temp, NUM_INSERTED);
        if(bytes_read == 0) {
            break;
        }

        // wait for space in buffer
        mutex_lock(&buffer->mutex);
        while (buffer->size + NUM_INSERTED > BUFFER_SIZE) {
            cv_wait(&buffer->full_cv, &buffer->mutex);
        }

        for(i = 0; i < bytes_read; i += 1) {
            buffer->buf[buffer->tail] = temp[i];
            buffer->tail = (buffer->tail + 1) % BUFFER_SIZE;
        }
        buffer->size += bytes_read;

        // signal consumers waiting for queue to be non-empty
        cv_bcast(&buffer->empty_cv);
        mutex_unlock(&buffer->mutex);
    }

    buffer->num_producers -= 1;
    close(fd);
    return;
}

void consume(buffer_t *buffer)
{
    int i, num_to_consume, local_sum = 0;

    while (1) {
        // wait while the queue is empty
        mutex_lock(&buffer->mutex);
        while (buffer->size - NUM_CONSUMED < 0 && buffer->num_producers) {
            cv_wait(&buffer->empty_cv, &buffer->mutex);
        }

        num_to_consume = (buffer->num_producers == 0) ? buffer->size : NUM_CONSUMED;
        for (i = 0; i < num_to_consume; i += 1) {
            local_sum += buffer->buf[buffer->head];
            buffer->buf[buffer->head] = 0;
            buffer->head = (buffer->head + 1) % BUFFER_SIZE;
        }

        buffer->size -= num_to_consume;
        if (!(buffer->num_producers))
            break;

        cv_bcast(&buffer->full_cv);
        mutex_unlock(&buffer->mutex);
    }
    
    buffer->checksum += local_sum;
    mutex_unlock(&buffer->mutex);
}
